<?php
/**
 * @file *
 * Wildfire core main module
 *
 * @author Craig Jones <craig@tiger-fish.com>
 * @author Chris Cohen <chris.cohen@tiger-fish.com>
 */

define('WILDFIRE_DATE', 'd M Y');
define('WILDFIRE_DATETIME', 'd M Y H:i');

define('WILDFIRE_TOKEN_START', '{{');
define('WILDFIRE_TOKEN_END', '}}');

define('WILDFIRE_UNSUBSCRIBE_LENGTH', 8);

define('WILDFIRE_STATUS_SUBSCRIBED', 1);
define('WILDFIRE_STATUS_UNSUBSCRIBED', 2);

define('WILDFIRE_LIST_STATUS_DISABLED', 0);
define('WILDFIRE_LIST_STATUS_LIVE', 1);
define('WILDFIRE_LIST_STATUS_TEST', 2);

define('WILDFIRE_MAX_TOP_LISTS', 5);
define('WILDFIRE_MAX_TEST_LISTS', 5);

/**
 * Perform a recursive array merge.
 *
 * Overwrites values from array 2 into array 1 only for keys that already exist
 * in array 1. Any keys in array2 that are not in array1 are discarded.
 *
 * e.g.
 *
 * $array1 = array(
 *   'fruit' => array(
 *     'apples' => '1',
 *     'oranges' => '1',
 *     'bananas' => '1',
 *   )
 * );
 *
 * $array2 = array(
 *   'fruit' => array(
 *     'apples' => '2',
 *     'oranges' => '2',
 *     'pears' => '2',
 *   )
 *   'veg' => array(
 *     'lettuce' => '2',
 *     'potatoes' => '2',
 *   )
 * );
 *
 * $array1 = array(
 *   'fruit' => array(
 *     'apples' => '2',
 *     'oranges' => '2',
 *     'bananas' => '1',
 *   )
 * );
 *
 * @param array $array1
 *   Array to merge.
 * @param array $array2
 *   Array to merge.
 *
 * @return array
 *   Merged array.
 */
function wildfire_array_overwrite_values(&$array1, &$array2) {

  $result = array();

  foreach ($array1 as $key => $value) {
    if (is_array($value)) {
      $result[$key] = wildfire_array_overwrite_values($array1[$key], $array2[$key]);
    }
    else {
      if (isset($array2[$key])) {
        // Use the new value.
        $result[$key] = $array2[$key];
      }
      else {
        // Use the existing value.
        $result[$key] = $array1[$key];
      }
    }
  }

  return $result;

}

/**
 * Render an email suitable to be displayed in a browser.
 *
 * @param int $jid
 *   The job ID of the job to be shown.
 * @param string $token
 *   The unsubscribe token assigned to this user when the job was sent. This
 *   ensures that the URL for a particular user to view a message is not
 *   guessable, preventing malicious unsubscribes.
 * @param bool $return
 *   Defaults to FALSE. If set to TRUE, causes the output to be returned
 *   through the theme system. If set to FALSE, causes the output to be sent
 *   directly, omitting the current theme.
 *
 * @return string
 *   The HTML version of the message for the specified job, rendered for the
 *   specified user.
 */
function wildfire_browserview($jid, $token = '', $return = FALSE) {

  try {
    $job = new WildfireClientJob($jid);
  }
  catch (Exception $e) {
    watchdog(
      'wildfire',
      'wildfire_browserview for @jid failed: @message',
      array(
        '@jid' => $jid,
        '@message' => $e->getMessage(),
      ),
      WATCHDOG_ERROR
    );
    return '';
  }

  // Assign a totally random token if no token was provided.
  if ($token == '') {
    $token = wildfire_random_string(WILDFIRE_UNSUBSCRIBE_LENGTH);
  }

  $ret = module_invoke_all('wildfire_browserview', $job->type, $jid, $token);

  // The return value should only contain one array element, because only one
  // of the modules should have actually rendered a preview, so just shift one
  // element off the array and use it as the preview.
  //
  // If you want a return value instead of outputting straight to the browser,
  // pass a bool(TRUE) as the last parameter :)
  $return = $return === TRUE ? TRUE : FALSE;
  if ($return) {
    return array_shift($ret);
  }

  print array_shift($ret);
  exit(0);
}

/**
 * Take a person's name and an email address, and form a recipient string.
 *
 * @param string $name
 *   The name of the person such as John Smith.
 * @param string $address
 *   A valid email address.
 *
 * @return string
 *   A recipient string such as "John Smith" <john.smith@example.com>, or
 *   FALSE if the address is invalid.
 */
function wildfire_construct_email_address($name = '', $address = '') {
  // The email address must be valid.
  if (!wildfire_valid_email_address($address)) {
    watchdog('wildfire', 'wildfire_construct_email_address was given an invalid email address: @email', array('@email' => $address), WATCHDOG_ERROR);
    return FALSE;
  }

  $parts = array();

  // Only include the name part if it's been provided, and wrap it in double
  // quotes.
  if (drupal_strlen($name) > 0) {
    $parts[] = '"' . $name . '"';
  }

  $parts[] = '<' . $address . '>';
  return implode(' ', $parts);
}

/**
 * Implements hook_wildfire_admin_block().
 */
function wildfire_wildfire_admin_block() {
  $output = array();

  $output[] = wildfire_quota_admin_block();

  return $output;
}

/**
 * Elapsed time.
 *
 * Pass a return value of microtime() to this function, either the string or
 * float value, and it will return a string with the amount of time in seconds
 * (to the nearest 1000th of a second) between that time and now.
 *
 * @param string|float $start
 *   The starting time.
 *
 * @return string
 *   The time since the starting time.
 */
function wildfire_elapsed_time($start) {
  $end = microtime(TRUE);
  if (is_string($start)) {
    $start = preg_match('/^0\\.[0-9]+ [0-9]+$/', $start)
      ? (float) preg_replace('/^(0\\.[0-9]+) ([0-9]+)$/', '$2.$1', $start) :
      FALSE;
  }
  if (is_string($end)) {
    $start = preg_match('/^0\\.[0-9]+ [0-9]+$/', $end)
      ? (float) preg_replace('/^(0\\.[0-9]+) ([0-9]+)$/', '$2.$1', $end) :
      FALSE;
  }
  if (!is_float($start) || !is_float($end)) {
    return 'UNKNOWN';
  }
  $elapsed = $end - $start;
  return (string) round($elapsed, 3);
}

/**
 * Indicate an error by using drupal_set_message() and writing to the watchdog.
 *
 * @param string $msg
 *   The message, which will be passed through t().
 * @param array $params
 *   An array of parameters to be passed to the t() function.
 * @param string $goto
 *   The URL to which to redirect the user. Optional.
 */
function wildfire_error($msg = 'No message', $params = array(), $goto = FALSE) {
  $dsm_msg = t($msg, $params);
  drupal_set_message(check_plain($dsm_msg), 'error');
  watchdog('wildfire', $msg, $params, WATCHDOG_ERROR);

  if ($goto !== FALSE) {
    drupal_goto($goto);
  }
}

/**
 * Count the number of lines in a given file.
 *
 * This function avoids using the file() method, meaning that it can be used
 * safely to count very large files, without encountering memory errors.
 *
 * @param string $filename
 *   The name of the file to open.
 *
 * @return int
 *   The number of lines in the file, or boolean FALSE if the file could not
 *   be opened. Note that this function could return 0 for a file that was
 *   opened successfully but is just empty, so make sure to distinguish between
 *   0 and boolean FALSE.
 */
function wildfire_get_lines_in_file($filename) {
  $handle = fopen($filename, "r");
  $lines = 0;

  if ($handle === FALSE) {
    return FALSE;
  }

  while (fgets($handle)) {
    $lines++;
  }

  fclose($handle);
  return $lines;
}

/**
 * Takes a time in seconds and converts it to a human-readable format.
 *
 * @param int $secs
 *   The number of seconds to be converted.
 *
 * @return string
 *   The human-readable length of time, such as 5 minutes and 33 seconds.
 */
function wildfire_human_readable_time($secs = 0) {
  $mins = 0;

  if ($secs > 59) {
    $mins = floor($secs / 60);
    $secs -= $mins * 60;
  }

  $pieces = array();

  // Only show minutes and seconds on the output if there ARE minutes or seconds
  // to avoid stuff like 3 minutes and 0 seconds.
  if ($mins > 0) {
    $pieces[] = format_plural($mins, '1 minute', '@count minutes');
  }
  if ($secs > 0) {
    $pieces[] = format_plural($secs, '1 second', '@count seconds');
  }

  return implode(' ' . t('and') . ' ', $pieces);
}

/**
 * Implements hook_menu().
 */
function wildfire_menu() {
  $items = array();

  // Admin menu.
  $items['admin/wildfire'] = array(
    'title' => 'Wildfire',
    'description' => 'Overview of Wildfire',
    'access arguments' => array('use wildfire'),
    'page callback' => 'wildfire_overview_page',
    'file' => 'wildfire.admin.inc',
  );
  $items['admin/wildfire/settings'] = array(
    'title' => 'Settings',
    'description' => 'Configure Wildfire',
    'access arguments' => array('administer wildfire global settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_settings_form'),
    'file' => 'wildfire.admin.inc',
  );
  $items['admin/wildfire/settings/general'] = array(
    'title' => 'General settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'description' => 'Configure general Wildfire settings',
    'access arguments' => array('administer wildfire global settings'),
    'page callback' => 'wildfire_settings_form',
    'file' => 'wildfire.admin.inc',
    'weight' => 0,
  );

  // Autocomplete callback to get nodes.
  $items['wildfire/autocomplete/node'] = array(
    'title' => 'Node autocomplete',
    'description' => 'Autocomplete callback to get nodes',
    'access callback' => TRUE,
    'page callback' => 'wildfire_node_autocomplete',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Callback to autocomplete a node title.
 */
function wildfire_node_autocomplete($text) {
  $matches = array();
  $types = variable_get('wildfire_content_types', array());
  $mytypes = array();

  // Go through each type and if we're allowed to send this type, add it to the
  // list of types in the SQL WHERE clause.
  foreach ($types as $type) {
    if ($type) {
      $mytypes[] = $type;
    }
  }

  // Build a list of parameters that we will pass to the SQL query.
  $params = $mytypes;
  $params[] = $text;

  if (!empty($mytypes)) {
    $query = db_select('node', 'n')
      ->fields('n', array('title')
      )
      ->where(
        "LOWER(n.title) LIKE LOWER('%" . check_plain($text) . "%')"
      )
      ->condition('type', $mytypes, 'IN')
      ->execute();

    while ($row = $query->fetchAssoc()) {
      $matches[$row['title']] = $row['title'];
    }

  }
  else {
    $matches = array();
  }

  print drupal_json_encode($matches);
  exit();
}

/**
 * Implements hook_node_delete().
 */
function wildfire_node_delete($node) {
  module_load_include('inc', 'wildfire_messages', 'wildfire_messages.regions');
  return wildfire_repeater_remove_node_all($node->nid);
}

/**
 * Implements hook_permission().
 */
function wildfire_permission() {
  return array(
    'administer wildfire global settings' => array(
      'title' => t('administer wildfire global settings'),
      'description' => t('Allows global settings to be changed in Wildfire'),
    ),
    'use wildfire' => array(
      'title' => t('use wildfire'),
      'description' => t('Allows use of Wildfire to send broadcasts'),
    ),
  );
}

/**
 * Callback to display a preview of an email.
 *
 * The function invokes hook_wildfire_preview which contacts each module
 * providing a mail type and asks it for a preview. Only the module implementing
 * the particular mail type we've asked for will respond.
 *
 * @param string $type
 *   The type of mailout being previewed.
 * @param int $extra
 *   The extra information corresponding to the type being previewed. This is
 *    usually the ID of the broadcast, alert, etc.
 * @param string $template
 *   The machine-readable name of the template to be used.
 */
function wildfire_preview($type = 'broadcast', $extra = 1, $template = '', $return = FALSE) {
  $ret = module_invoke_all('wildfire_preview', $type, $extra, $template);

  // The return value should only contain one array element, because only one
  // of the modules should have actually rendered a preview, so just shift one
  // element off the array and use it as the preview.
  //
  // If you want a return value instead of outputing straight to the browser,
  // pass a bool(TRUE) as the last parameter :)
  $return = $return === TRUE ? TRUE : FALSE;
  if ($return) {
    return array_shift($ret);
  }

  print array_shift($ret);
  exit(0);
}

/**
 * Generates a random password.
 *
 * @return string
 *   The random password.
 */
function wildfire_random_password() {
  $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!"£$%^&*()_-+';
  $numchars = drupal_strlen($chars);

  // Random password length between 6 and 16 characters.
  $passlen = rand(6, 16);

  $pass = '';

  // Generate the password, one character at a time.
  for ($i = 0; $i < $passlen; $i++) {
    $pass .= drupal_substr($chars, rand(0, $numchars - 1), 1);
  }

  return $pass;
}

/**
 * Generates a random string.
 *
 * @param int $length
 *   The length of the random string to create.
 *
 * @return string
 *   The random string.
 */
function wildfire_random_string($length = 0) {
  $c = 0;
  $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  $charend = drupal_strlen($chars) - 1;
  $token = '';

  while ($c < $length) {
    $token .= drupal_substr($chars, rand(0, $charend), 1);
    $c++;
  }

  return $token;
}

/**
 * Generates a valid, unique username from an users email address.
 *
 * @param string $email_address
 *   The email address of the user to generate a username for.
 *
 * @return string
 *   The new username.
 */
function wildfire_generate_username($email_address = '') {
  // 2011-08-19, craig: TODO: optimize this further.
  // Lower case the email address entirely before we start, in case the end
  // user entered something like: JoeBloggs@example.com
  $email_address = strtolower($email_address);

  $prefix = drupal_substr($email_address, 0, strpos($email_address, '@'));

  if ($prefix == FALSE) {
    // $email_address isn't a valid email address.
    if (!empty($email_address)) {
      // Use whatever was passed in.
      $prefix = $email_address;
    }
    else {
      // As we can't derive a nice value from $email_address as it's empty,
      // use a random value instead by recycling wildfire_random_password().
      $prefix = wildfire_random_password();
    }
  }
  elseif (ctype_digit((string) $prefix)) {
    // Catch cases where the prefix would be fully numeric. Use a random
    // value instead so that we don't end up with all-numeric usernames.
    $prefix = wildfire_random_password();
  }

  // We only want alphabet characters in the $prefix, in case our requested
  // prefix already contains numbers or other characters.
  $prefix = preg_replace('/[^A-Za-z]/', '', $prefix);

  // Possible optimized way of finding the username we want to use:
  // 1/ Get user where user is $prefix
  // 2/ If we don't find our prefix, it's free, just use it, we don't need
  // to hunt a free username out.
  // 3/ If we do, find all users with our $prefix. Strip off our
  // $prefix, and we should be left with either a number, or a string.
  // 4/ If a string, skip it (username = $prefix . 'something')
  // 5/ If a number, set to highest found prefix if > current highest
  // 6/ +1, and this should result in a unique prefix+number username,
  // which we can then use.
  //
  // QUESTION IS... is this going to be quicker than multiple SQL queries?
  // it should be. At most, we are performing two SQL queries.
  //
  // Find existing user on exact match.
  $query = db_select('users', 'u')
    ->fields('u', array('name')
    )
    ->condition('name', $prefix, '=')
    ->execute();

  // Check the return in lower case. This ensures that if we search for 'admin',
  // and the database returns 'Admin', it's still validly "found" an existing
  // user.
  //
  // You can't create two users with the same username but different case, as
  // the SQL server will still treat then as the same unique key.
  if (strtolower($query->fetchField()) !== $prefix) {
    // Prefix isn't already used as a username. We've found our result.
    return $prefix;
  }
  else {
    $query = db_select('users', 'u')
      ->fields('u', array('name'));
    $query->condition('name', $prefix . '%', 'LIKE');
    $result = $query->execute();

    // Collate all existing usernames that start with $prefix
    // Check through each to find the highest numerical suffix
    // This should be quicker than doing separate SQL queries to
    // determine the same.
    $highest_suffix = 0;
    while ($row = $result->fetchAssoc()) {
      $suffix = drupal_substr($row['name'], drupal_strlen($prefix));
      if (!empty($suffix) && ctype_digit((string) $suffix)) {
        if ($suffix > $highest_suffix) {
          $highest_suffix = $suffix;
        }
      }
    }

    $new_username = $prefix . (string) ($highest_suffix + 1);

    return $new_username;
  }

}

/**
 * Implements hook_theme().
 */
function wildfire_theme() {
  $tpath = drupal_get_path('module', 'wildfire') . '/templates';

  return array(
    'wildfire_admin_page' => array(
      'render element' => 'form',
      'file' => 'wildfire.theme',
      'path' => $tpath,
    ),
    'wildfire_image' => array(
      'variables' => array(
        'template' => '',
        'path' => '',
        'alt' => '',
        'title' => '',
        'attributes' => NULL,
        'getsize' => TRUE,
      ),
      'file' => 'wildfire.theme',
      'path' => $tpath,
    ),
    'wildfire_image_path' => array(
      'variables' => array(
        'template' => '',
        'path' => '',
      ),
      'file' => 'wildfire.theme',
      'path' => $tpath,
    ),
    'wildfire_field_render' => array(
      'variables' => array(
        'entity' => array(),
        'field' => '',
        'display' => 'default',
        'delta' => 0,
        'entity_type' => 'node',
      ),
      'file' => 'wildfire.theme',
      'path' => $tpath,
    ),
    'wildfire_quota_block'  => array(
      'variables' => array(
        'quota' => NULL,
        'refresh_link' => NULL,
      ),
      'template' => 'wildfire_quota_block',
      'file' => 'wildfire.theme',
      'path' => $tpath,
    ),
  );
}

/**
 * Checks whether a specified user ID is valid.
 *
 * Statically cached.
 *
 * @param int $uid
 *   The user ID to check.
 *
 * @return bool
 *   TRUE if the user ID is valid. FALSE otherwise.
 */
function wildfire_valid_user($uid = 0) {
  static $valid_users;

  // Make sure the user ID is not something stupid.
  if ($uid < 1) {
    return FALSE;
  }

  if (!isset($valid_users[$uid])) {
    $query = db_select('users', 'u')
      ->fields('u', array('uid'));
    $query->condition('uid', $uid, '=');
    $result = $query->execute();

    $valid_users[$uid] = $result->fetchField() === FALSE ? FALSE : TRUE;
  }

  return $valid_users[$uid];
}
/**
 * Helper function to determine the maximum permissable upload size.
 *
 * This is more intelligent than simply performing an ini_get(), as it
 * doesn't assume that the values in php.ini are in K.
 *
 * @param bool $human_readable
 *   If TRUE, returns a human-readable formatted max size.
 *
 * @return string
 *   Max size in bytes as a string, or a pre-formatted size string
 *   depending on $human_readable flag
 */
function wildfire_get_max_upload_size($human_readable = FALSE) {

  $pms = ini_get('post_max_size');
  $pms_v = drupal_substr($pms, 0, -1);

  switch (drupal_substr($pms, -1)) {
    case 'G':
    case 'g':
      $pms_v *= 1073741824;
      break;

    case 'M':
    case 'm':
      $pms_v *= 1048576;
      break;

    case 'K':
    case 'k':
      $pms_v *= 1024;
      break;
  }

  $mus = ini_get('upload_max_filesize');
  $mus_v = drupal_substr($mus, 0, -1);

  switch (drupal_substr($mus, -1)) {
    case 'G':
    case 'g':
      $mus_v *= 1073741824;
      break;

    case 'M':
    case 'm':
      $mus_v *= 1048576;
      break;

    case 'K':
    case 'k':
      $mus_v *= 1024;
      break;
  }

  // Use whatever is the smaller of post_max_size / upload_max_filesize as any
  // upload won't be able to exceed that value in any case.
  if ($mus_v > $pms_v) {
    $max_size = $pms_v;
  }
  else {
    $max_size = $mus_v;
  }

  if ($human_readable) {
    return format_size($max_size);
  }
  else {
    return $max_size;
  }

}

/**
 * Processes links in content, and converts them from relative to absolute.
 *
 * @param string $content
 *   Input HTML content.
 *
 * @return string
 *   Output HTML content, where all 'src=' or 'href=' attributes are converted
 *  to absolute paths.
 */
function wildfire_convert_relative_links($content) {

  $preg_result = preg_replace_callback(
    '/(src|href)="([^"]+)"/i',
    'wildfire_convert_relative_links_cb',
    $content
  );

  return $preg_result;

}

/**
 * Callback for wildfire_convert_relative_links().
 */
function wildfire_convert_relative_links_cb($matches) {

  global $base_url;

  $modified = $matches[1] . '="';

  if (strpos($matches[2], '://') === FALSE && strpos($matches[2], 'mailto:') === FALSE) {
    // It's relative URL!
    //
    // Strip off any leading slash, so that we are left with a consistent
    // path (we assume that any path starting without a slash should
    // be relative to the root in any case).
    if (drupal_substr($matches[2], 0, 1) == '/') {
      $matches[2] = drupal_substr($matches[2], 1);
    }

    $modified .= $base_url . '/' . $matches[2];
  }
  else {
    // It's absolute URL, so reconstruct as-is and leave it alone.
    $modified .= $matches[2];
  }

  $modified .= '"';

  return $modified;

}

/**
 * Takes a number of seconds and formats it as Xh, Xm, Xs.
 *
 * @param int $seconds
 *   Number of seconds.
 *
 * @return string
 *   Formatted duration string.
 */
function wildfire_format_duration($seconds) {

  $s = $seconds % 60;
  $m = ($seconds / 60) % 60;
  $h = ($seconds / 60 / 60) % 60;

  return sprintf('%dh, %dm, %ds', $h, $m, $s);

}

/**
 * Verify the syntax of the given e-mail address.
 *
 * We use this within wildfire rather than the built-in valid_email_address()
 * function, as internally that uses...
 *
 * filter_var($mail, FILTER_VALIDATE_EMAIL);
 *
 * ...except that filter_var with FILTER_VALIDATE_EMAIL behaves differently
 * depending on the version of PHP installed.
 *
 * e.g. "invalid..address@example.com" is accepted on PHP 5.3.2, but rejected
 * on PHP 5.3.10
 *
 * https://bugs.php.net/bug.php?id=53395
 *
 * The RegEx below is a copy of the regex used prior to Drupal switching to the
 * filter_var() method, with a small modification to reject addresses with a
 * trailing dot.
 *
 * FIXME: Whilst this makes the behaviour consistent, it's not as good a RegEx
 * which is why it was replaced in Drupal in the first place
 *
 * Empty e-mail addresses are allowed. See RFC 2822 for details.
 *
 * @param string $mail
 *   A string containing an e-mail address.
 *
 * @return bool
 *   TRUE if the address is in a valid format.
 */
function wildfire_valid_email_address($mail) {
  $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
  $domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+[^\.]';
  $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
  $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';

  return preg_match("/^$user@($domain|(\[($ipv4|$ipv6)\]))$/", $mail);
}

/**
 * Provides a block that displays the systems current quota on overview page.
 *
 * @param bool $no_links
 *   If set, no admin links will appear in the block. Defaults to FALSE.
 */
function wildfire_quota_admin_block() {

  drupal_add_css(
    drupal_get_path('module', 'wildfire') . '/css/wildfire.quota-block.css',
    array('preprocess' => FALSE)
  );

  $wildfire_quota = new WildfireQuota;

  $quota['total']        = $wildfire_quota->getTotal();
  $quota['last_checked'] = $wildfire_quota->getLastChecked();
  $quota['top-up']       = $wildfire_quota->getTopup();
  $quota['monthly']      = $wildfire_quota->getMonthly();

  $content = array(
    '#theme' => 'wildfire_quota_block',
    '#quota' => $quota,
  );

  return array(
    'title' => t('Quota'),
    'description' => t('The send quota for your current system'),
    'content' => drupal_render($content),
    'show' => TRUE,
  );

}