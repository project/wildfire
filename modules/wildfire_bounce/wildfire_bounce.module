<?php
/**
 * @file
 * Wildfire core bounce mechanisms and functions.
 */

define('WILDFIRE_BOUNCE_RESULT_NOT_FOUND', 1);
define('WILDFIRE_BOUNCE_RESULT_ALREADY_BOUNCED', 2);
define('WILDFIRE_BOUNCE_RESULT_BOUNCED', 3);

/**
 * Add a new bounced user.
 *
 * @param string $email
 *   The email address to be marked as bounced.
 * @param string $module
 *   The name of the module executing this bounce.
 * @param string $message
 *   The message given by the recipient's MTA (mail transfer agent) as the
 *   reason why this bounce occurred.
 *
 * @return int
 *   One of the following values:
 *   - WILDFIRE_BOUNCE_RESULT_NOT_FOUND: this email address is not in this
 *     system.
 *   - WILDFIRE_BOUNCE_RESULT_ALREADY_BOUNCED: email address is in the system
 *     but is already marked as bounced.
 *   - WILDFIRE_BOUNCE_RESULT_BOUNCED: email address has been marked as bounced.
 */
function wildfire_bounce_user_add($email, $module, $message) {
  // A bit of sanity checking.
  if (empty($email)) {
    return WILDFIRE_BOUNCE_RESULT_NOT_FOUND;
  }

  // Check that this email address corresponds to a user on the system.
  $uid = db_select('users', 'u')
    ->fields('u', array('uid'))
    ->condition('u.mail', $email)
    ->execute()
    ->fetchField();

  // If the user doesn't exist, log the attempt but go no further.
  if (empty($uid)) {
    return WILDFIRE_BOUNCE_RESULT_NOT_FOUND;
  }

  // Check that this email address is not already bounced.
  if (wildfire_bounce_is_bounced($email)) {
    return WILDFIRE_BOUNCE_RESULT_ALREADY_BOUNCED;
  }

  // Add an entry to the optouts table.
  // TODO: This is a temporary solution. Ideally this should not be an optout,
  // but should be a proper bounce and the mailer should take this into account.
  db_merge('wildfire_optouts')
    ->key(array(
      'lid' => 0,
      'uid' => $uid,
    ))
    ->fields(array(
      'lid'           => 0,
      'timestamp'     => time(),
      'method'        => 3,
      'unsubscriber'  => 1,
    ))
    ->execute();

  // Add the bounce.
  db_insert('wildfire_bounces')
    ->fields(array(
      'email'     => $email,
      'timestamp' => time(),
      'module'    => $module,
      'message'   => $message,
    ))
    ->execute();

  return WILDFIRE_BOUNCE_RESULT_BOUNCED;
}

/**
 * Check whether an email is bounced already.
 *
 * @param string $email
 *   The email address to check.
 */
function wildfire_bounce_is_bounced($email) {
  $timestamp = db_select('wildfire_bounces', 'wb')
    ->fields('wb', array('timestamp'))
    ->condition('wb.email', $email)
    ->execute()
    ->fetchField();

  return $timestamp === FALSE ? FALSE : TRUE;
}
