<?php
/**
 * @file
 * Manages bounces that come from the Amazon SES bounce notifier.
 */

/**
 * Implements hook_menu().
 */
function wildfire_bounce_amazonses_menu() {
  $items = array();

  $items['wildfire/bounce/amazonses'] = array(
    'title'           => 'Amazon SES bounce report',
    'page callback'   => 'wildfire_bounce_amazonses_page',
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Page callback to receive a JSON message from Amazon SNS.
 */
function wildfire_bounce_amazonses_page() {
  $input = file_get_contents('php://input');
  $sns = json_decode($input);

  // $sns->Type must be set for this to be considered a valid notification.
  if (!isset($sns->Type)) {
    watchdog('wildfire_bounce_amazonses', 'Bad SNS notification received: <pre>!raw</pre>', array(
      '!raw'  => $input,
    ));
    exit();
  }

  // If this is a subscription confirmation request, make sure we process it.
  if ($sns->Type == 'SubscriptionConfirmation') {
    wildfire_bounce_amazonses_subscribe($sns);
  }
  elseif ($sns->Type == 'Notification') {
    // Only process this if it's a bounce notification.
    $msg = json_decode($sns->Message);

    if (isset($msg->notificationType) && $msg->notificationType == 'Bounce') {
      if (isset($msg->bounce->bounceType)) {
        if ($msg->bounce->bounceType == 'Permanent') {
          // Grab the destination addresses (there might be more than one) and
          // try to process them all.
          $recipients = $msg->bounce->bouncedRecipients;

          foreach ($recipients as $recipient) {
            $result = wildfire_bounce_user_add(
              $recipient->emailAddress,
              'wildfire_bounce_amazonses',
              $recipient->diagnosticCode
            );

            // Record the result.
            switch ($result) {
              case WILDFIRE_BOUNCE_RESULT_NOT_FOUND:
                watchdog('wildfire_bounce_amazonses', 'Email %email not found, not marked as bounced: <pre>!raw</pre>', array(
                  '%email'  => $recipient->emailAddress,
                  '!raw'    => $input,
                ));
                break;

              case WILDFIRE_BOUNCE_RESULT_ALREADY_BOUNCED:
                watchdog('wildfire_bounce_amazonses', 'Email %email already marked as bounced, ignored: <pre>!raw</pre>', array(
                  '%email'  => $recipient->emailAddress,
                  '!raw'    => $input,
                ));
                break;

              case WILDFIRE_BOUNCE_RESULT_BOUNCED:
                watchdog('wildfire_bounce_amazonses', 'Email %email has been marked as bounced: <pre>!raw</pre>', array(
                  '%email'  => $recipient->emailAddress,
                  '!raw'    => $input,
                ));
                break;

              default:
                watchdog('wildfire_bounce_amazonses', 'Unrecognised status returned from wildfire_bounce_user_add(): <pre>!raw</pre>', array(
                  '!raw'    => $input,
                ));
                break;
            }
          }
        }
        else {
          // Bounce type was not permanent. Let's log this but do nothing
          // further.
          watchdog('wildfire_bounce_amazonses', 'SNS non-permanent bounce notification ignored: <pre>!raw</pre>', array(
            '!raw'  => $input,
          ));
        }
      }
      else {
        // This was a bounce notification that contained no bounce subtype, so
        // ignore it.
        watchdog('wildfire_bounce_amazonses', 'SNS notification ignored: did not contain a bounce subtype: <pre>!raw</pre>', array(
          '!raw'  => $input,
        ));
      }
    }
    else {
      // This was a notification, but not a bounce notification, so ignore it.
      watchdog('wildfire_bounce_amazonses', 'Generic SNS notification received and ignored: <pre>!raw</pre>', array(
        '!raw'  => $input,
      ));
    }
  }
  else {
    watchdog('wildfire_bounce_amazonses', 'Generic SNS message received: <pre>!raw</pre>', array(
      '!raw'  => $input,
    ));
  }

  exit();
}

/**
 * Subscribe to a particular SNS notification.
 *
 * Subscriptions are required when you first set up a new SNS notification, and
 * unless confirmed, it will not continue to send notifications to this URL.
 */
function wildfire_bounce_amazonses_subscribe($sns) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $sns->SubscribeURL);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_exec($ch);

  $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  watchdog('wildfire_bounce_amazonses', 'Attempted to subscribe to SNS notification, status was @status', array(
    '@status' => $status_code,
  ));
}
