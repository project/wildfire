<?php
/**
 * @file
 *  wildfire_auth.test
 *
 * Tests the Wildfire client auth key
 */
module_load_include('test', 'wildfire', 'tests/wildfire');

/**
 * Test the auth key system
 */
class WildfireAuthTestCase extends WildfireWebTestCase {

  /**
   * Implementation of getInfo() method.
   */
  public static function getInfo() {
    return array(
      'name' => 'Auth - Main',
      'description' => 'Tests the auth keys functionality of the Wildfire system',
      'group' => 'Wildfire',
    );
  }

  /**
   * Implementation of setUp()
   */
  public function setUp() {
    parent::setUp('wildfire_auth');

    // Create some extra permissions needed for this module
    $permissions = array(
      'administer wildfire auth',
      'administer wildfire global settings'
    );

    // Add a new role with these permissions
    $new_role = $this->drupalCreateRole($permissions);

    // Add this additional role to the user account.
    $this->user->roles[$new_role] = $new_role;
    user_save($this->user);

    // Log in again to ensure the permissions are being picked up
    $this->drupalLogin($this->user);

  }

  /**
   * Implementation of tearDown()
   */
  public function tearDown() {
    parent::tearDown();
  }


  /**
   * Runs the tests
   */
  public function testAuth() {

     // Check the Wildfire system settings page shows our modifications
    $this->checkUI();

  }

  // Check the Wildfire system settings page shows our modifications
  public function checkUI() {

    /**
     * Load the Wildfire system settings page, and check our additions are
     * shown on the form
     */
    $this->drupalget('admin/wildfire/settings');
    $this->assertFieldByName('wildfire_rpc_authkey');

  }

}
