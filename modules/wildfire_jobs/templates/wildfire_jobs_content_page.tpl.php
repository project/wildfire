<?php
/**
 * @file wildfire_jobs_content_page.tpl.php
 *   Provides a template for viewing a the content of a job
 */
?>
<iframe id="wildfire-preview-iframe" src="<?php print $iframe_src; ?>" style="width: 100%; height: 500px;">
</iframe>
