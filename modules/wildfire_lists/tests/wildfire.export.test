<?php
/**
 * @file
 * Tests the export system.
 */
module_load_include('test', 'wildfire', 'tests/wildfire');

/**
 * Test the reporting system.
 */
class WildfireExportTestCase extends WildfireWebTestCase {

  /**
   * Implementation of getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Lists - Export',
      'description' => 'Test the export functionality of Wildfire',
      'group' => 'Wildfire',
    );
  }

  public function setUp() {
    // Generic setup, eg, user, etc.

    parent::setUp(array('field'));

    /**
     * Add some fields. Most live sites will be setup with at least some
     * extra fields on the user entity, so we add some to emulate this
     * allowing us to check for their presence in the export.
     */

    $fields = array(
      'field_first_name' => array(
        'label' => 'First name',
        'description' => '',
        'required' => FALSE,
        'field_name' => 'field_first_name',
        'type' => 'text',
        'default_value' => 'Default First Name',
        'module' => 'text',
        'entity_type' => 'user',
        'bundle' => 'user',
      ),
      'field_last_name' => array(
        'label' => 'Last name',
        'description' => '',
        'required' => FALSE,
        'field_name' => 'field_last_name',
        'type' => 'text',
        'default_value' => 'Default Last Name',
        'module' => 'text',
        'entity_type' => 'user',
        'bundle' => 'user',
      ),
    );

    $existing_fields = field_info_instances('user');

    // Unset new field definition for any field that already exists...
    foreach ($existing_fields['user'] as $existing_field) {
      unset($fields[$existing_field['field_name']]);
    }

    // ... then setup anything that remains
    foreach ($fields as $field_name => $field) {
      field_create_field($field);
      $instance = array(
        'label' => $field['label'],
        'field_name' => $field['field_name'],
        'entity_type' => $field['entity_type'],
        'bundle' => 'user',
      );
      field_create_instance($instance);
    }

    // Create some extra permissions needed for this module
    $permissions = array(
      'administer wildfire lists',
      'use wildfire lists',
    );

    // Add a new role with these permissions
    $new_role = $this->drupalCreateRole($permissions);

    // Add this additional role to the user account.
    $this->user->roles[$new_role] = $new_role;
    user_save($this->user);

    // Log in again to ensure the permissions are being picked up
    $this->drupalLogin($this->user);

  }

  /**
   * Main test function for the reports.
   */
  public function testExport() {
    // Test if setup failed.
    if ($this->preFail !== FALSE) {
      $preFail = is_string($this->preFail)
        ? $this->preFail
        : 'Setup failed. Test aborted.';
      $this->fail($preFail);
      return;
    }

    $start = microtime();

    // List the tests here.
    $this->exportListPage();
    $this->exportForm();
    $this->exportCSV();

    $this->pass(t(
      'Tests completed in !time seconds',
      array('!time' => $this->elapsedTime($start))
    ));
  }

  /**
   * Test for the presence of the 'export' button on the lists list page.
   *
   */
  protected function exportListPage() {
    $this->generateList();
    $this->drupalGet('admin/wildfire/lists');

    $xpath = $this->xpath('//table[@id="wildfire-lists"]/tbody/tr[1]/td[5]/a[4]');
    $this->assertEqual(
      $xpath[0], 'export',
      t('Export button found on page')
    );
  }

  /**
   * Test that the export form is being rendered correctly.
   */
  protected function exportForm() {
    $list = $this->generateList();
    $this->drupalGet('admin/wildfire/lists/' . $list->id . '/export');

    // Check the summary at the top.
    $this->assertText('You are about to export from the ' . $list->name . ' list');
    $this->assertText('a total of 2 users');

    // Check the checkboxes.
    $this->assertText('Subscribed (2)');
    $this->assertText('Unsubscribed (0)');

    $this->assertFieldById('edit-submit');

    // Post the form with no checkboxes checked. Make sure it bails.
    $params = array(
      'statuses[' . WILDFIRE_STATUS_SUBSCRIBED . ']' => FALSE,
      'statuses[' . WILDFIRE_STATUS_UNSUBSCRIBED . ']' => FALSE,
    );
    $this->drupalPost('admin/wildfire/lists/' . $list->id . '/export', $params, t('Export'));
    $this->assertText('You must check at least one status to be exported.');
  }

  protected function exportCSV() {
    // Create a list with 3 users. Unsubscribe one of these users.
    $list = $this->generateList(3);
    $timestamp = REQUEST_TIME;

    // Get the list entries
    $list_entries = wildfire_list_get($list->id, TRUE);

    // Reset default first name and last name on a user's profile to random
    // values.
    $account = user_load($list_entries[0]['uid']);

    if (module_exists('field')) {
      $firstname = $this->randomName(16);
      $lastname = $this->randomName(16);

      $edit['field_first_name']['und'][0]['value'] = $firstname;
      $edit['field_last_name']['und'][0]['value'] = $lastname;

      user_save($account, $edit);
    }
    else {
      $this->fail(t('"field" module is not available. Some tests will fail'));
    }

    // ...and opt the first generated user out
    wildfire_list_add_optout(0, $account->uid, $timestamp, 1, $this->user->uid);

    // Post the export form, with subscribers and unsubscribers checked.
    $post = array(
      'lid' => $list->id,
      'statuses[' . WILDFIRE_STATUS_SUBSCRIBED . ']' => WILDFIRE_STATUS_SUBSCRIBED,
      'statuses[' . WILDFIRE_STATUS_UNSUBSCRIBED . ']' => WILDFIRE_STATUS_UNSUBSCRIBED,
    );
    $this->drupalPost(
      'admin/wildfire/lists/' . $list->id . '/export',
      $post,
      t('Export')
    );

    // Make sure the headers are present in the output.
    $this->assertText(
      '"User ID","Username","Email","Status","Opted out"',
      t('Found headers in CSV output')
    );

    // Make sure there are exactly 4 newlines in the output.
    $this->assertEqual(
      substr_count($this->content, "\n"),
      4,
      t('Correct number of lines (4) found in CSV')
    );

    // Make sure the field info is in the output.
    if (module_exists('field')) {
      $this->assertText(
        '"First name"',
        t('Field information found')
      );
    }

    // Post the export form, with just subscribers this time.
    $post = array(
      'lid' => $list->id,
      'statuses[' . WILDFIRE_STATUS_SUBSCRIBED . ']' => WILDFIRE_STATUS_SUBSCRIBED,
    );
    $this->drupalPost(
      'admin/wildfire/lists/' . $list->id . '/export',
      $post,
      t('Export')
    );

    // Make sure there are exactly 4 newlines in the output.
    $this->assertEqual(
      substr_count($this->content, "\n"),
      3,
      t('Correct number of lines (3) found in CSV')
    );

    // Post the export form, with just unsubscribers this time.
    $post = array(
      'lid' => $list->id,
      'statuses[' . WILDFIRE_STATUS_UNSUBSCRIBED . ']' => WILDFIRE_STATUS_UNSUBSCRIBED,
    );
    $this->drupalPost(
      'admin/wildfire/lists/' . $list->id . '/export',
      $post,
      t('Export')
    );

    // Make sure there are exactly 4 newlines in the output.
    $this->assertEqual(
      substr_count($this->content, "\n"),
      2,
      t('Correct number of lines (2) found in CSV')
    );

    // Make sure the field info and timestamp are in the output.
    if (module_exists('field')) {

      $users = wildfire_list_get_users($list->id);

      $this->verbose(print_r($users, TRUE));

      $this->assertText(
        '"First name"',
        t('Field header found')
      );

      $this->assertText('"' . $firstname . '"', t('First name found'));
      $this->assertText('"' . $lastname . '"', t('Last name found'));
    }

    $this->assertText(
      '"' . format_date($timestamp, 'custom', WILDFIRE_DATETIME) . '"',
      t('Timestamp found')
    );
  }
}
