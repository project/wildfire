<?php
/**
 * @file
 *    Theme functions for the wildfire module.
 */

/**
 * Preprocess function for theme_wildfire_list_selector.
 */
function template_preprocess_wildfire_list_selector(&$vars) {
  module_load_include('inc', 'wildfire_lists', 'wildfire_lists.api');

  // Get list names along with active subscriber counts.
  $alllists = wildfire_lists_get();
  $vars['lists'] = array();

  foreach (array_keys($alllists) as $list) {
    $totals = wildfire_list_get_totals($list);
    $vars['lists'][$list] = array(
      'name' => $totals['name'],
      'count' => $totals[WILDFIRE_STATUS_SUBSCRIBED],
    );
  }

  $vars['helptext'] = t(
    'Please choose the list that should be used. !editlink.',
    array(
    '!editlink' => l(
        t('Edit or add lists'),
        'admin/wildfire/lists'
      ),
  )
  );
  $vars['removelink'] = t('Change');
  $vars['removehelp'] = t('Click to clear the list selection and choose another.');
}

/**
 * Custom theme function for step 2 of the import process.
 *
 * This is the step where the user maps CSV columns to user profile fields.
 */
function theme_wildfire_lists_import_form_step2($variables) {
  $form = $variables['form'];
  $header = array();

  // If the column names and sample column values are available, use an extra
  // column in the table to show the sample.
  if (isset($form['sample'])) {
    $header = array(
      t('Field'),
      t('Sample'),
      t('Import to'),
    );
  }
  else {
    $header = array(
      t('Field'),
      t('Import to'),
    );
  }

  $output = drupal_render($form['linecount']);
  $output .= drupal_render($form['importhelp']);
  $rows = array();

  foreach (element_children($form['field']) as $key) {
    $row = array();

    // Field.
    $row[] = drupal_render($form['field'][$key]);

    // Sample. Only if one was available.
    if (isset($form['sample'])) {
      $row[] = drupal_render($form['sample'][$key]);
    }

    $row[] = drupal_render($form['importdest'][$key]);

    $rows[] = $row;
  }

  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Custom theme function for step 3 of the import process.
 *
 * This is the step where the batch import results are displayed.
 */
function theme_wildfire_lists_import_form_step3($variables) {
  $form = $variables['form'];
  $output = '';
  $results = variable_get('wildfire_batch_results', t('No batch results could be found.'));
  // Since ALL variables are loaded each page request, and we could potentially
  // have thousands of items in the array, so delete the entire variable once
  // we have made a copy, since we only created it to gain some persistence
  // at the end of the batch.
  variable_del('wildfire_batch_results');

  if (!empty($results)) {
    // Prepare a table for the results.

    if (!is_array($results)) {
      $result = $results;
      $results = array();
      $results[] = $result;
    }

    $headers = array(
      t('Result'),
      t('Count'),
    );

    $rows = array();

    foreach ($results as $key => $value) {
      $rows[] = array(
        $key,
        count($results[$key]),
      );
    }

    $output .= theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array(), 'caption' => t('Import results')));
  }

  // Prepare a table for the failures, if there were any.
  if (!empty($results['failed'])) {

    // Some errors are just a simple string. Place in array[0] so that the
    // rows can be built
    if (!is_array($results['failed'])) {
      $failed = $results['failed'];
      $results = array();
      $results['failed'][] = $failed;
    }

    $headers = array(
      t('Line'),
      t('Reason'),
    );

    $rows = array();

    foreach ($results['failed'] as $line => $reason) {
      $rows[] = array(
        $line,
        $reason,
      );
    }

    $output .= theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array(), 'caption' => t('Failed rows')));
  }

  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Theme function for list rows
 */
function theme_wildfire_list_row($variables) {

  if (empty($variables['row'])) {
    return '<tr colspan="3" id="list-empty-row">'
           . '<td class="col1">' . t('List is empty') . '</td>'
           . '</tr>';
  }
  else {
    return '<tr class="even" id="list-row-' . $variables['uid'] . '">'
           . '<td class="col1">' . $variables['row'][0] . '</td>'
           . '<td class="col2">' . $variables['row'][1] . '</td>'
           . '<td class="col3">' . $variables['row'][2] . '</td>'
           . '</tr>';
  }

}
