<?php
/**
 * @file
 *  wildfire_broadcasts.module
 *
 * Manages Wildfire broadcasts
 *
 * @author Craig Jones <craig@tiger-fish.com>
 *
 * Code derived from Wildfire 1:
 * @author Chris Cohen <chris.cohen@tiger-fish.com>
 */

/**
 * Implements hook_menu().
 */
function wildfire_broadcasts_menu() {
  $items = array();

  /**
   * Broadcasts
   */
  $items['admin/wildfire/broadcasts'] = array(
    'title' => 'Broadcasts',
    'description' => 'A list of broadcasts',
    'access arguments' => array('use wildfire messages'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_list_form'),
    'file' => 'wildfire_broadcasts.admin.inc',
  );
  $items['admin/wildfire/broadcasts/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'description' => 'A list of broadcasts',
    'access arguments' => array('use wildfire messages'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_list_form'),
    'file' => 'wildfire_broadcasts.admin.inc',
    'weight' => 0,
  );
  $items['admin/wildfire/broadcasts/add'] = array(
    'title' => 'Add',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Add a broadcast',
    'access arguments' => array('administer wildfire messages'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_form'),
    'file' => 'wildfire_broadcasts.admin.inc',
    'weight' => 1,
  );
  $items['admin/wildfire/broadcasts/%'] = array(
    'title' => 'Broadcast',
    'title callback' => 'wildfire_title_broadcast',
    'title arguments' => array(3),
    'type' => MENU_CALLBACK,
    'description' => 'Configure a broadcast',
    'access arguments' => array('administer wildfire messages'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_form', 3),
    'file' => 'wildfire_broadcasts.admin.inc',
  );
  $items['admin/wildfire/broadcasts/%/config'] = array(
    'title' => 'Config',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'description' => 'Configure a broadcast',
    'access arguments' => array('administer wildfire messages'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_form', 3),
    'file' => 'wildfire_broadcasts.admin.inc',
    'weight' => 1,
  );
  $items['admin/wildfire/broadcasts/%/delete'] = array(
    'title' => 'Delete',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Delete a broadcast',
    'access arguments' => array('administer wildfire messages'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_delete_form', 3),
    'file' => 'wildfire_broadcasts.admin.inc',
    'weight' => 2,
  );
  $items['admin/wildfire/broadcasts/%/send'] = array(
    'title' => 'Send',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Send a broadcast',
    'access arguments' => array('use wildfire messages'),
    'page callback' => 'wildfire_broadcasts_send_page',
    'page arguments' => array('wildfire_broadcasts_send_form', 3),
    'file' => 'wildfire_broadcasts.admin.inc',
    'weight' => 3,
  );

  /**
   * Regions
   */
  if (module_exists('wildfire_messages')) {
    /**
     * All of these menu entries hook directly into the code in
     * wildfire_messages to avoid duplication.
     */
    $mpath = drupal_get_path('module', 'wildfire_messages');

    $items['admin/wildfire/broadcasts/%/content'] = array(
      'title' => 'Content',
      'type' => MENU_LOCAL_TASK,
      'description' => 'Add content to or remove content from the broadcast',
      'access arguments' => array('administer wildfire messages'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('wildfire_content_overview_form', 'broadcast', 3),
      'file' => 'wildfire_messages.regions.inc',
      'file path' => $mpath,
      'weight' => 0,
    );
    $items['admin/wildfire/broadcasts/%/content/overview'] = array(
      'title' => 'Overview',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'description' => 'An overview of the broadcast\'s content',
      'access arguments' => array('use wildfire messages'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('wildfire_content_overview_form', 'broadcast', 3),
      'file' => 'wildfire_messages.regions.inc',
      'file path' => $mpath,
    );
    $items['admin/wildfire/broadcasts/%/content/repeaters'] = array(
      'title' => 'Repeaters',
      'type' => MENU_LOCAL_TASK,
      'description' => 'Add or reorder content on a repeater',
      'access arguments' => array('administer wildfire messages'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('wildfire_repeater_edit_form', 'broadcast', 3),
      'file' => 'wildfire_messages.regions.inc',
      'file path' => $mpath,
    );
    /**
     * NOTE: This menu path is 9 elements long. This is the max for Drupal 7.
     * The max for Drupal 6 is only 7, so you may need to refactor this
     * if the code is ever backported.
     */
    $items['admin/wildfire/broadcasts/%/content/repeaters/remove/%/%'] = array(
      'title' => 'Remove',
      'type' => MENU_CALLBACK,
      'description' => 'Remove a node from a repeater',
      'access arguments' => array('administer wildfire messages'),
      'page callback' => 'wildfire_repeater_delete_node',
      'page arguments' => array('broadcast', 3, 7, 8),
      'file' => 'wildfire_messages.regions.inc',
      'file path' => $mpath,
    );
    $items['admin/wildfire/broadcasts/%/content/snippets'] = array(
      'title' => 'Snippets',
      'type' => MENU_LOCAL_TASK,
      'description' => 'Add or edit content on a snippet',
      'access arguments' => array('administer wildfire messages'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('wildfire_snippet_edit_form', 'broadcast', 3),
      'file' => 'wildfire_messages.regions.inc',
      'file path' => $mpath,
    );

  }

  /**
   * Repeaters
   */
  $items['admin/wildfire/broadcasts/get_repeaters/%/%'] = array(
    'title' => 'Get repeaters',
    'type' => MENU_CALLBACK,
    'description' => 'Gets repeaters for selected broadcast',
    'access arguments' => array('use wildfire messages'),
    'page callback' => 'wildfire_broadcast_get_repeaters',
    'page arguments' => array(4, 5),
    'file' => 'wildfire_broadcasts.admin.inc',
  );

  /**
   * Node: add to broadcast
   */
  $items['node/%/add_to_broadcast'] = array(
    'title' => 'Add to broadcast',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Add this node to a broadcast.',
    'access callback' => 'wildfire_allowed_content_types',
    'access arguments' => array(1),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wildfire_broadcasts_add_node_to_repeater_form', 1),
    'file' => 'wildfire_broadcasts.admin.inc',
    'weight' => 2,
  );

  return $items;
}

/**
 * Implements hook_wildfire_type_info().
 *
 * This hook should return an array of strings matching the $type field of any
 * classes implementing WildfireClientMessageInterface within this module.
 * The corresponding class will get loaded depending on this $type field when
 * requested (via the WildfireClientMessage::getMessageObject method).
 */
function wildfire_broadcasts_wildfire_type_info() {
  return array('broadcast' => t('Broadcast'));
}

/**
 * Implements hook_wildfire_message_metadata().
 */
function wildfire_broadcasts_wildfire_message_metadata($message) {
  if (!empty($message['type']) && $message['type'] == 'broadcast') {
    module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');
    return wildfire_broadcast_metadata($message);
  }
  else {
    return FALSE;
  }
}

/*
 * Obtains the Broadcasts block used on the Wildfire Overview page.
 *
 * @return array
 *  Renderable block for use on by wildfire_overview_page()
 */
function wildfire_broadcasts_admin_block() {

  module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');

  $headers = array(
    t('ID'),
    t('Name'),
    t('Last Sent'),
    t('Operations'),
  );

  $broadcasts = wildfire_broadcasts_get(3);

  $rows = array();

  foreach ($broadcasts as $mid => $broadcast) {

    // Build the operations that are possible for each broadcast.
    $operations = array();
    $operations[] = l(
      t('content'),
      'admin/wildfire/broadcasts/' . $mid . '/content',
      array('attributes' => array('title' => t('Add content to or remove content from this broadcast')))
    );
    $operations[] = l(
      t('preview & send'),
       'admin/wildfire/broadcasts/' . $mid . '/send',
      array('attributes' => array('title' => t('Preview in your browser, then choose a list and send this broadcast')))
    );
    $operations = implode(' | ', $operations);

    $rows[] = array(
      'mid' => check_plain($mid),
      'name' => check_plain($broadcast['name']),
      'lastsent' => $broadcast['lastsent'] == 0 ? '…' : format_date($broadcast['lastsent'], 'custom', WILDFIRE_DATETIME),
      'operations' => $operations,
    );

  }

  $content = theme(
    'table',
    array(
      'header' => $headers,
      'rows' => $rows,
      'attributes' => array(
        'id' => 'wildfire-latest-broadcasts-table'
      )
    )
  );

  $content .= l(
    t('View full list'),
    'admin/wildfire/broadcasts',
    array('attributes' => array(
      'title' => t('View a full list of broadcasts'),
      'class' => array('wildfire-overview-view-full'),
    ))
  );

  return array(
    'title' => t('Latest broadcasts'),
    'description' => t('The latest broadcasts that have been created.'),
    'content' => $content,
    'show' => TRUE,
  );

}

/**
 * Gets the metadata for a broadcast with the specified ID.
 *
 * @param int $message
 *    An array representing a database row for the message being requested.
 * @return array
 *    An array of metadata for the broadcast
 *
 * TODO: Full exception handling
 */
function wildfire_broadcast_metadata($message) {

  $message = new WildfireBroadcast($message['mid']);

  $metadata = array(
    'headers' => array(
      'subject' => $message->subject,
      'from_name' => $message->from_name,
      'from_email' => $message->from_email,
      'reply_name' => $message->reply_name,
      'reply_email' => $message->reply_email,
    ),
    'name' => $message->name,
  );

  return $metadata;

}

/**
 * Implements hook_wildfire_message_populate().
 *
 * @param array $message
 *  The message array.
 *  @see _wildfire_broadcasts_process_row()
 *
 * @return array
 *  The message array, populated with region data, or $message untouched
 */
function wildfire_broadcasts_wildfire_message_populate($message) {
  if (!empty($message['type']) && $message['type'] == 'broadcast') {
    module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');
    return wildfire_broadcast_populate($message);
  }
  else {
    return $message;
  }
}

/**
 * Implements hook_wildfire_message_delete_regions().
 *
 * @param string $type
 *  The type of message being deleted
 * @param int $mid
 *  The message ID of the message to delete regions from
 */
function wildfire_broadcasts_wildfire_message_delete_regions($type, $mid) {

  if (!empty($type) && $type == 'broadcast') {

    // Delete any attached repeaters
    db_delete('wildfire_repeaters_nodes')
      ->condition('mid', $mid, '=')
      ->execute();

    // Delete any attached snippets
    db_delete('wildfire_snippets_content')
      ->condition('mid', $mid, '=')
      ->execute();

  }

}

/**
 * Implements hook_theme().
 */
function wildfire_broadcasts_theme() {
  $tpath = drupal_get_path('module', 'wildfire_broadcasts') . '/templates';

  return array(
    'wildfire_broadcasts_send_page' => array(
      'variables' => array(
        'form' => '',
        'preview' => array(
          'type' => '',
          'mid' => 0,
          'template' => ''
        )
      ),
      'file' => 'wildfire_broadcasts.theme',
      'path' => $tpath,
    ),
    'wildfire_broadcasts_list_form' => array(
      'render element' => 'form',
      'file' => 'wildfire_broadcasts.theme',
      'path' => $tpath,
    ),
  );

}

/**
 * Implements hook_wildfire_browserview().
 */
function wildfire_broadcasts_wildfire_browserview($type = '', $jid, $token) {
  /**
   * If the $type requested is not 'broadcast', the request is not for us
   * but for another module that implements the requested type.
   */
  if ($type != 'broadcast') {
    return NULL;
  }

  module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');
  module_load_include('inc', 'wildfire_jobs', 'wildfire_jobs.send');
  module_load_include('inc', 'wildfire_jobs', 'wildfire_jobs.api');

  // Use the token to find which user it belongs to.
  $uid = wildfire_job_get_uid_from_token($token);

  // Deal with a situation where the token was not found.
  if ($uid === FALSE) {
    $uid = 0;
  }

  /**
   * Load the message content from a Job ID. This loads the state of the
   * message object at the point that the job was created, rather than the
   * current state.
   */
  $message = new WildfireBroadcast();
  $message->loadFromJob($jid);

  if ($uid == 0) {
    /**
     * If the UID is zero, then the token given does not represent a registered
     * user. As a result, any generated tokens will be garbage. Change these to
     * read as '#ANON_USER'
     */
    $token_list = $message->getTokenList();
    $token_list = array_keys($token_list);
    $token_list = array_flip($token_list);
    foreach ($token_list as $key => &$value) {
      $value = '#ANON_USER';
    }

    $message->setTokenReplacements($token_list);

  }
  else {
    // For any valid user, populate the full set of tokens
    $message->populateTokenReplacements($jid, $uid, FALSE);
  }

  return $message->getContentWithTokenReplacements(WILDFIRE_MESSAGE_FORMAT_HTML);

}

/**
 * Implements hook_wildfire_message_get().
 *
 * @param string $type
 *  The type of message to retrieve. Only 'broadcast' is accepted for this
 *  module
 * @param int $mid
 *  The message ID to retrieve
 *
 * @return array
 *  A message array representing the broadcast, or FALSE if the broadcast was
 *  not found.
 *  @see _wildfire_broadcasts_process_row()
 */
function wildfire_broadcasts_wildfire_message_get($type, $mid) {
  if ($type == 'broadcast') {
    module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');
    return wildfire_broadcast_get($mid);
  }
}

/**
 * Implements hook_wildfire_preview().
 *
 * @param string $type
 *  The type of message to retrieve. Only 'broadcast' is accepted for this
 *  module
 * @param int $mid
 *  The message ID to retrieve
 * @param string $template
 *  The template to render the preview with. This parameter is not applicable
 *  to broadcasts, as they will always render with the template definition
 *  stored in the message object.
 *
 * @return string
 *  The rendered preview as a string, or NULL if the $type passed in is not
 *  'broadcast'
 */
function wildfire_broadcasts_wildfire_preview($type = '', $mid = 0, $template = '') {
  /**
   * If the $type requested is not 'broadcast', the request is not for us
   * but for another module that implements the requested type.
   */
  if ($type != 'broadcast') {
    return NULL;
  }

  module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');
  module_load_include('inc', 'wildfire_jobs', 'wildfire_jobs.send');
  module_load_include('inc', 'wildfire_messages', 'wildfire_messages.template.api');

  $message = WildfireClientMessage::getMessageObject($type);
  $message->load($mid);

  // Make sure the specified template is valid.
  $valid_template = wildfire_template_get($message->template);

  if ($valid_template === FALSE) {
    return t('Unable to load template %template.', array('%template' => $message->template));
  }

  // Prepare the message content.
  $message->render();

  // Set up all the token replacements to be '#TEST'
  $token_list = $message->getTokenList();
  $token_list = array_keys($token_list);
  $token_list = array_flip($token_list);
  foreach ($token_list as $key => &$value) {
    $value = '#TEST';
  }

  // Poke these back into the WildfireMessage class
  $message->setTokenReplacements($token_list);

  // Get the HTML body for the message
  $htmlbody = $message->getContentWithTokenReplacements();

  return $htmlbody;
}

/**
 * Implements hook_wildfire_admin_block().
 */
function wildfire_broadcasts_wildfire_admin_block() {
  $output = array();

  module_load_include('inc', 'wildfire_broadcasts', 'wildfire_broadcasts.api');
  $output[] = wildfire_broadcasts_admin_block();

  return $output;
}
