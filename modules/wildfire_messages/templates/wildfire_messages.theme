<?php
/**
 * @file
 *    Theme functions for the wildfire module.
 */

/**
 * Preprocess function for theme_wildfire_template_regions.
 *
 * Splits the regions in the template out into their types, and lists them.
 */
function template_preprocess_wildfire_template_regions(&$vars) {
  $vars['regions'] = array();

  if (!empty($vars['template']['regions']) && is_array($vars['template']['regions'])) {
    foreach ($vars['template']['regions'] as $rname => $region) {
      $content = array();

      foreach ($region as $zone) {
        $content[] = '<em>' . $zone['title'] . '</em>';
      }

      $vars['regions'][] = array(
        'name' => ucfirst($rname),
        'content' => implode(', ', $content),
      );
    }
  }
}

/**
 * Preprocess function for theme_wildfire_template_selector.
 */
function template_preprocess_wildfire_template_selector(&$vars) {
  module_load_include('inc', 'wildfire_messages', 'wildfire_messages.template');

  // Render the XHTML code for the template's image.
  foreach ($vars['templates'] as $name => &$template) {
    $template['image'] = theme(
      'image',
      array(
        'path' => $template['screenshot'],
        'alt' => t('Preview of the template @template', array('@template' => $template['title'])),
        'title' => t('Click to select this template.'),
      )
    );
  }
}

/**
 * Theme function to render a broadcast order page, including draggable nodes.
 */
function theme_wildfire_repeater_edit_form($variables) {
  $form = $variables['form'];
  $output = '';
  $header = array(
    t('Order'),
    t('Title'),
  );

  // Search the form for fieldsets that we need to render tables into.
  foreach (element_children($form) as $element) {
    if (substr($element, strlen($element) - 18, 18) == '_repeater_fieldset') {
      // This form element is a fieldset, so let's work with it.
      $rname = substr($element, 0, strlen($element) - 18);
      $rows = array();

      if (!empty($form[$element]['title'])) {
        foreach (element_children($form[$element]['title']) as $id) {
          if (is_array($form[$element]['title'][$id])) {
            $row = array();
            $row[] = drupal_render($form[$element]['title'][$id]);
            $row[] = drupal_render($form[$element]['weight']['weight-' . $rname . '-' . $id]);

            $rows[] = array(
              'class' => array(
                'draggable',
              ),
              'data' => $row,
            );
          }
        }
      }

      if (!count($rows)) {
        $row = array();
        $row[] = array(
          'colspan' => count($header),
          'data' => t('This repeater has no content yet.'),
        );
        $rows[] = $row;
      }

      $form[$element]['table'] = array(
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'repeater-' . $rname . '-order-nodes'))),
      );
      drupal_add_tabledrag('repeater-' . $rname . '-order-nodes', 'order', 'sibling', 'node-order-weight');
    }
  }

  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Theme function for wildfire_template_list_form().
 */
function theme_wildfire_template_list_form($variables) {
  $form = $variables['form'];
  $output = '';

  $header = array(
    t('Preview'),
    t('Title'),
    t('Enabled'),
    t('Settings'),
  );

  $templates = wildfire_templates_get();
  $rows = array();

  foreach ($templates as $name => $template) {
    $rows[] = array(
      drupal_render($form['screenshot'][$name]),
      drupal_render($form['template_title'][$name]) . drupal_render($form['description'][$name]),
      drupal_render($form['status'][$name]),
      drupal_render($form['settings'][$name]),
    );
  }

  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'wildfire-template-list')));

  $output .= drupal_render($form['submit']);
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Custom theme function to display a selector for mail templates.
 *
 * Each template is shown as a picture on which the user may click to select it.
 * @param $templates array
 *    The array of mail templates.
 */
function theme_wildfire_template_selector($variables) {
  $templates = $variables['templates'];
  $helptext = '<div class="description">' . t('Click on a template below to select it.') . '</div>';

  return $helptext;
}

