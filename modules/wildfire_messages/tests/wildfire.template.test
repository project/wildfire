<?php
/**
 * @file
 * Tests the templating system.
 */
module_load_include('test', 'wildfire', 'tests/wildfire');

class WildfireTemplateTestCase extends WildfireWebTestCase {

  /**
   * Get Information
   *
   * @access protected
   * @return array
   *  Information about this test
   */
  public static function getInfo() {
    return array(
      'name' => 'Messages - Templates',
      'description' => 'Test the Template functionality of the Wildfire module.',
      'group' => 'Wildfire',
    );
  }

  /**
   * Setup
   *
   * @access public
   * @return void
   */
  public function setUp() {
    // Generic setup, eg, user, etc.
    parent::setUp();

    // Create some extra permissions needed for this module
    $permissions = array(
      'use wildfire templates',
      'administer wildfire templates',
    );

    // Add a new role with these permissions
    $new_role = $this->drupalCreateRole($permissions);

    // Add this additional role to the user account.
    $this->user->roles[$new_role] = $new_role;
    user_save($this->user);

    // Log in again to ensure the permissions are being picked up
    $this->drupalLogin($this->user);
  }

  /**
   * Template Tests
   *
   * @access public
   * @return void
   */
  public function testTemplates() {
    // Test if setup failed.
    if ($this->preFail !== FALSE) {
      $preFail = is_string($this->preFail)
        ? $this->preFail
        : 'Setup failed. Test aborted.';
      $this->fail($preFail);
      return;
    }

    $start = microtime();

    // List the tests here.
    $this->templatePaths();
    $this->templateAPI();
    $this->templatePage();
    $this->templateSettingsPage();

    $this->pass(t(
      'Tests completed in !time seconds',
      array('!time' => $this->elapsedTime($start))
    ));
  }

  /**
   * Template Test: Template Paths
   *
   * Scans the various possible templates paths.
   *
   * @access protected
   * @return void
   */
  protected function templatePaths() {
    $theme = variable_get('theme_default', 'garland');

    // Define our list of places that we should look for mailtemplates in.
    $paths = wildfire_templates_get_paths($theme);

    // Iterate through the paths, and check each one. But I guess you didn't
    // really need me to explain what a foreach does, do you?
    $found_path = FALSE;

    foreach ($paths as $path) {
      if (is_dir($path)) {
        $this->pass(t(
          'Mail templates folder exists at %path.',
          array('%path' => $path)
        ));
        $found_path = TRUE;
      }
    }

    // If we weren't able to find any template folders, flag this as an error.
    if (!$found_path) {
      $this->fail(t(
        'No templates folders found in any locations. Locations were: %paths',
        array('%paths' => implode(', ', $paths))
      ));
    }
  }

  /**
   * Test some template API functionality.
   *
   * For this, we need to assume that the Wildfire Example template exists, is
   * available for this test, and that it has one repeater called main, and one
   * snippet called editors_note. If your tests are failing, check that these
   * things are all true.
   */
  protected function templateAPI() {
    // Start by clearing out any template information we might already have.
    $tables = array(
      'wildfire_templates',
      'wildfire_templates_regions',
      'wildfire_repeaters',
      'wildfire_repeaters_nodes',
      'wildfire_snippets',
      'wildfire_snippets_content',
      'wildfire_templates_settings',
    );
    foreach ($tables as $table) {
      db_query("TRUNCATE TABLE {" . $table . "}");
    }

    $templates = wildfire_templates_refresh();

    $this->assertTrue(
      is_array($templates['wildfire_example_newsletter']),
      t('The Wildfire Example template was found for testing')
    );
    $this->assertEqual(
      $templates['wildfire_example_newsletter']['regions']['repeaters']['main']['title'],
      'Main',
      t('Main repeater found')
    );
    $this->assertEqual(
      $templates['wildfire_example_newsletter']['regions']['snippets']['editors_note']['title'],
      'Editor\'s Note',
      t('Editor\'s Note snippet found')
    );

    $settings = wildfire_templates_get_settings('wildfire_example_newsletter', 0);

    $this->assertEqual(
      $settings['colors']['title'],
      '#000',
      t('Default color setting for title found')
    );
    $this->assertEqual(
      $settings['colors']['background'],
      '#FFF',
      t('Default color setting for background found')
    );

  }

  /**
   * Test the template list page.
   *
   * Note this relies on the Wildfire Example template being available to the test.
   */
  protected function templatePage() {
    // Get the templates list and make sure the test template shows up.
    $this->drupalGet('admin/wildfire/templates');

    // The following relies on us knowing that a specific template being
    // present.
    $this->assertNoText(t('The mailtemplates folder was not found'));
    $this->assertText(t('Wildfire Example'));
    $this->assertText('Snippets:');
    $this->assertText('Editor\'s Note');
    $this->assertText('Repeaters:');
    $this->assertText('Main');
    $this->assertFieldById('edit-status-wildfire-example-newsletter', 0);

    // Check the status checkbox, submit the form, and check that it gets
    // saved properly.
    $enable = array(
      'status[wildfire_example_newsletter]' => 1,
    );
    $this->drupalPost('admin/wildfire/templates', $enable, t('Save'));
    $this->assertFieldById('edit-status-wildfire-blue', 1);

    $query = db_select('wildfire_templates', 'ct')
      ->fields('ct', array(
      'status',
    ));
    $query->condition('name', 'wildfire_example_newsletter', '=');
    $result = $query->execute();

    $status = $result->fetchField();

    $this->assertEqual(1, $status, t('Template was enabled successfully'));

    // Count the number of templates, and make sure there is a row in the
    // template list for each template.
    $templates = wildfire_templates_get(TRUE);
    $tcount = count($templates);
    $xpath = $this->xpath('//table[@id="wildfire-template-list"]/tbody/tr');
    $xcount = count($xpath);
    $this->assertEqual(
      $xcount,
      $tcount,
      t('There are @num templates on the template list (@actual)', array('@num' => $tcount, '@actual' => $xcount))
    );
  }

  /**
   * Test the template settings page.
   *
   * Note this relies on the Wildfire Example and Wildfire Plain templates being
   * available to the test.
   */
  protected function templateSettingsPage() {

    // Get the template settings for Wildfire Plain
    $this->drupalGet('admin/wildfire/templates/wildfire_plain/settings');

    // Check there's no settings
    $this->assertText(
      t('The "wildfire_plain" template has no settings that can be configured'),
      t('No settings text found')
    );

    // Get the template settings for Wildfire Example
    $this->drupalGet('admin/wildfire/templates/wildfire_example_newsletter/settings');

    // Check the settings form contain all of the required fields
    $settings = wildfire_templates_get_settings('wildfire_example_newsletter', 0);

    // We will store the first field name, so we can use it later
    $first_valid_field = '';

    foreach ($settings as $section => $svalue) {
      foreach ($svalue as $key => $value) {
        if (empty($first_valid_field)) {
          $first_valid_field = 'settings_' . $section . '_' . $key;
        }
        $this->assertFieldByName('settings_' . $section . '_' . $key, $value);
      }
    }

    $this->assertFieldByName('op', 'Save');

    // Submit a change to the first field, and check it was saved correctly.
    $fields = array(
      $first_valid_field => $this->randomName(6),
    );
    $this->drupalPost('admin/wildfire/templates/wildfire_example_newsletter/settings', $fields, t('Save'));

    // Assert we got a success message.
    $this->assertText(
      t(
        'Settings for "!template" saved',
        array(
          '!template' => 'wildfire_example_newsletter'
        )
      )
    );

    // Get the template settings for Wildfire Example again
    $this->drupalGet('admin/wildfire/templates/wildfire_example_newsletter/settings');

    // Check that the field we just edited shows the correct value
    $this->assertFieldByName($first_valid_field, $fields[$first_valid_field]);

  }

}
