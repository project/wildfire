<?php
/**
 * @file
 * Provides a template for rendering the quota block.
 */
?>

<div class="wildfire-quota-block">
  <dl>
    <dt>Total available send quota:</dt><dd><?php print $quota['total'];?></dd>
  </dl>
</div>
