------------------------------------
-- Wildfire 2 client for Drupal 7 --
------------------------------------

== WHAT IT IS ==

Wildfire is an email marketing system that integrates fully with your website
to allow simple, fast and intuitive bulk emails to be sent to your subscribers
in just a couple of clicks. Any regular Drupal content can be dropped straight
into an email with ease.

== WHAT IT DOES ==

The client module provides mail template, list management, content management,
and job tracking tools that tie in tightly with your Drupal site.

If the content (like a news story or blog post) already exists in your site,
you just choose which stories to include in your email, and Wildfire takes care
of constructing the email.

It collates send "job" information and sends it to our Wildfire HQ servers. We
take care of managing the actual sending of the bulk mail via our dedicated
whitelisted mail servers, so delivery rates are much higher.

Use of this module therefore requires an account with Wildfire HQ to actually
perform a mail out. To obtain a free account, please visit our Wildfire HQ
website (http://wildfirehq.co.uk/)

== MAINTAINERS AND SUPPORT ==

The Wildfire client and Wildfire HQ mail sending services are provided by
Tigerfish Interactive (http://tiger-fish.com/).

If you have any issues with the sending service itself that is not a bug in the
client, or just feel like getting in touch, please contact us!
